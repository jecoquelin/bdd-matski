##sqlwb.pos=5995
##sqlwb.selStart=5995
##sqlwb.selEnd=5995
create schema matski_update;
set schema 'matski_update';

/*====================*/
/*  Table : ARTICLE   */
/*====================*/
create table ARTICLE (
  numArticle        integer     not null,
  nomArticle        varchar(50) null,
  referenceInternet char(10)    null,
  codeBarre         char(13)    null,
  coutAchat         numeric     null,
  constraint pk_article primary key (numArticle)
);

Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(14,'P','SLIDY','PA1236','3245673451456',4.5,3.2);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(14,'P','QUICKY','PA1237','3245674451456',3.5,3);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(14,'P','WEEZ 2','PA1238','3245673451457',19,16);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(14,'P','WAVE1','PA1239','3245673451458',12,10);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(14,'P','WEEZ  1','PA1240','3245673451460',10,8.2);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(14,'P','WAVE 2','PA1241','3245673451461',14,12);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(13,'P','YOONER','PA1242','3245673451462',55,0);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','GHOST  90','PA1243','3245673451463',170,100);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','LIVE FIT 60','PA1244','3245673451464',100,60);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','LIVE FIT 70','PA1245','3245673451465',120,65);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','QUES MAX 110','PA1246','3245673451466',320,180);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','QUES MAX BC 120','PA1247','3245673451467',400,220);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','LIVE FIT 130','PA1248','3245673451468',220,130);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(5,'P','X PRO 110','PA1249','3245673451469',300,170);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(18,'P','Q-90','PA1250','3245673451470',350,280);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(18,'P','Q-LAB','PA1251','3245673451471',650,550);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(18,'P','Q-115','PA1252','3245673451472',500,380);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(18,'P','Q-85','PA1253','3245673451473',350,250);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(18,'P','Q-98','PA1254','3245673451474',400,350);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','EXPERIENCE 83 TPX','PA1234','3245673451236',548,255);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','SMOKE','PA1255','3245673451475',400,230);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','VR27','PA1256','3245673451476',250,150);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','X-LANDER 6.0','PA1257','3245673451477',132.4,95.6);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','VR07','PA1258','3245673451478',500,300);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','AFFINITY AIR','PA1259','3245673451479',320,200);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','BBR 9.0','PA1260','3245673451480',500,380);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(15,'P','VANTAGE ALIBI','PA1261','3245673451481',548,200);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(21,'P','SL SERIE','PA1235','3245673451456',549,345);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(21,'P','XLT','PA1262','3245673451482',470,260);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(21,'P','SICKSTICK','PA1263','3245673451483',430,230);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(21,'P','RESERVE','PA1264','3245673451484',410,230);
Insert into ARTICLE (NUMCATEGORIE,CODETYPE,NOMARTICLE,REFERENCEINTERNE,CODEBARRE,PRIXVENTE,COUTACHAT ) values
(21,'P','DERVY','PA1265','3245673451485',300,160);


/*======================*/
/*  Table : CATEGORIE   */
/*======================*/

create table CATEGORIE (
  numCategorie      int         not null,
  libelleCate       varchar(40) null,
  constraint pk_categorie primary key (numCategorie)
);

Insert into CATEGORIE (,LIBELLECATEGORIE) values
(0,'Matériel ski alpins');
Insert into CATEGORIE (,LIBELLECATEGORIE) values
(0,'Matériel de snowboard');
Insert into CATEGORIE (,LIBELLECATEGORIE) values
(0,'Matériel ski nordique');
Insert into CATEGORIE (,LIBELLECATEGORIE) values
(0,'Matériel ski montagne');
Insert into CATEGORIE (,LIBELLECATEGORIE) values
(1,'Chaussures');
Insert into CATEGORIE (,LIBELLECATEGORIE) values
(1,'Skis');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(1,'Batons');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(2,'Chaussures');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(2,'Snowboards');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(3,'Chaussures');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(3,'Batons');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(3,'Skis');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(4,'Paret');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(4,'Luges');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(6,'Polyvalent');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(6,'Freestyle');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(6,'Racing');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(6,'Freeride');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(6,'Randonnée');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(9,'Polyvalent');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(9,'Freestyle');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(9,'Alpine');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(9,'Freeride');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(12,'Skating');
Insert into CATEGORIE (CAT_NUMCATEGORIE2,LIBELLECATEGORIE) values
(12,'Alternatif');
----------- WbStatement -----------
##sqlwb.pos=2057
##sqlwb.selStart=2057
##sqlwb.selEnd=2057
drop schema if exists matski_uptdate cascade;
create schema matski_update;
set schema 'matski_update';

/*
insert into
  matski_update.typeArticle
  (codeType, libelleType)
  
select codeType, libelleType
from matski.typeArticle
*/

/*====================*/
/*  Table : ARTICLE   */
/*====================*/
create table ARTICLE (
  numArticle        integer     not null,
  nomArticle        varchar(50) null,
  referenceInternet char(10)    null,
  codeBarre         char(13)    null,
  coutAchat         numeric     null,
  constraint pk_article primary key (numArticle)
);


/*======================*/
/*  Table : CATEGORIE   */
/*======================*/

create table CATEGORIE (
  numCategorie      int         not null,
  libelleCate       varchar(40) null,
  constraint pk_categorie primary key (numCategorie)
);


/*==================*/
/*  Table : CLIENT  */
/*==================*/

create table CLIENT (
  numClient               serial,
  nomClient               varchar(50),
  adresseRueClient        varchar(50),
  adresseCodePostalClient varchar(50),
  adresseVilleClient      varchar(40),
  adressePaysClient       varchar(30),
  telephoneClient         varchar(12),
  mailClient              varchar(60),
  constraint pk_client primary key (numClient)
);



/*====================*/
/*  Table : CONTACT   */
/*====================*/

create table CONTACT (
  numContact          serial          not null,
  nomContact          varchar(50)     not null,
  telephoneContact    varchar(12)     not null,
  fonctionContact     varchar(20)     not null,
  constraint pk_contact primary key (numClient)
);


/*====================*/
/*  Table : COMMANDE  */
/*====================*/

create table COMMANDE(
  numCommande         serial          not null,
  numClient           integer         not null,
  dateCommande        date            not null,
  montantFrais        numeric(10,2)   not null,
  montantHT           numeric(10,2)   not null,
  montantTTC          numeric(10,2)   not null,
  constraint pk_commande primary key (numCommande)
  CONSTRAINT fk_commande_appartien_client FOREIGN KEY (numclient)
  REFERENCES matski.client (numclient) 
);
----------- WbStatement -----------
##sqlwb.pos=3201
##sqlwb.selStart=3201
##sqlwb.selEnd=3201
drop schema if exists matski_uptdate cascade;
create schema matski_update;
set schema 'matski_update';

/*
insert into
  matski_update.typeArticle
  (codeType, libelleType)
  
select codeType, libelleType
from matski.typeArticle
*/

/*====================*/
/*  Table : ARTICLE   */
/*====================*/
create table ARTICLE (
  numArticle        integer     not null,
  nomArticle        varchar(50) null,
  referenceInternet char(10)    null,
  codeBarre         char(13)    null,
  coutAchat         numeric     null,
  constraint pk_article primary key (numArticle)
);


/*======================*/
/*  Table : CATEGORIE   */
/*======================*/

create table CATEGORIE (
  numCategorie      int         not null,
  libelleCate       varchar(40) null,
  constraint pk_categorie primary key (numCategorie)
);


/*==================*/
/*  Table : CLIENT  */
/*==================*/

create table CLIENT (
  numClient               serial,
  nomClient               varchar(50),
  adresseRueClient        varchar(50),
  adresseCodePostalClient varchar(50),
  adresseVilleClient      varchar(40),
  adressePaysClient       varchar(30),
  telephoneClient         varchar(12),
  mailClient              varchar(60),
  constraint pk_client primary key (numClient)
);



/*====================*/
/*  Table : CONTACT   */
/*====================*/

create table CONTACT (
  numContact          serial          not null,
  nomContact          varchar(50)     not null,
  telephoneContact    varchar(12)     not null,
  fonctionContact     varchar(20)     not null,
  constraint pk_contact primary key (numClient)
);


/*====================*/
/*  Table : COMMANDE  */
/*====================*/

create table COMMANDE(
  numCommande         serial          not null,
  numClient           integer         not null,
  dateCommande        date            not null,
  montantFrais        numeric(10,2)   not null,
  montantHT           numeric(10,2)   not null,
  montantTTC          numeric(10,2)   not null,
  constraint pk_commande primary key (numCommande)
);


/*==========================*/
/*  Table : DETAILCOMMANDE  */
/*==========================*/

create table DETAILCOMMANDE(
  
)


/*========================*/
/*  Table : TYPEARTICLE   */
/*========================*/

create table TYPEARTICLE(
  codeType          char(1)     not null,
  libelleType       varchar(40) null ?,
  constraint pk_typeArticle primary key (codeType)
);


/*============================*/
/*  Table : DETAILSCOMMANDE   */
/*============================*/

create table DETAILSCOMMANDE (
  numCommande           integer,
  numArticle            integer,
  quantiteCommandee     integer,
  quantiteLivree        integer,
  constraint pk_detailsCommande primary key (numCommandee, numArticle)
  constraint fk_detailscommande foreign key (numCommandee)
  REFERENCES COMMANDE (numCommande)
  constraint fk_detailscommande foreign key (numArticle)
  REFERENCES article (numArticle)
);


/*======================*/
/*  Table : ETIQUETTE   */
/*======================*/

create table ETIQUETTE(
  codeEtiquette         char(3)       not null,
  libelleEtiquette      varchar(70)   not null,
  codeTypeTVA           integer       not null,
  constraint pk_etiquette primary key (codeEtiquette)
);


/*======================*/
/*  Table : LISTEPRIX   */
/*======================*/

create table LISTEPRIX(
  codeListe           char(1),
  libelleliste        varchar(20),
  constraint pk_listePrix primary key (codeListe)
);


/*======================*/
/*  Table : TARIFVENTE  */
/*======================*/

DROP TABLE IF EXISTS matski.tarifvente CASCADE;

CREATE TABLE tarifvente
(
   numarticle  integer         NOT NULL,
   codeliste   char(1)         NOT NULL,
   prixvente   numeric(10,2)
   constraint pk_listePrix primary key (codeListe,numArticle)
   CONSTRAINT fk_listePrix foreign key (codeListe)
   REFERENCES listePrix (codeliste)
   Constraint fk_article (numArticle)
   REFERENCES article (numArticle)
);

----------- WbStatement -----------
##sqlwb.pos=2536
##sqlwb.selStart=2536
##sqlwb.selEnd=2536
drop schema if exists matski_uptdate cascade;
create schema matski_update;
set schema 'matski_update';

/*
insert into
  matski_update.typeArticle
  (codeType, libelleType)
  
select codeType, libelleType
from matski.typeArticle
*/

/*====================*/
/*  Table : ARTICLE   */
/*====================*/
create table ARTICLE (
  numArticle        integer     not null,
  nomArticle        varchar(50) null,
  referenceInternet char(10)    null,
  codeBarre         char(13)    null,
  coutAchat         numeric     null,
  constraint pk_article primary key (numArticle)
);


/*======================*/
/*  Table : CATEGORIE   */
/*======================*/

create table CATEGORIE (
  numCategorie      int         not null,
  libelleCate       varchar(40) null,
  constraint pk_categorie primary key (numCategorie)
);


/*==================*/
/*  Table : CLIENT  */
/*==================*/

create table CLIENT (
  numClient               serial,
  nomClient               varchar(50),
  adresseRueClient        varchar(50),
  adresseCodePostalClient varchar(50),
  adresseVilleClient      varchar(40),
  adressePaysClient       varchar(30),
  telephoneClient         varchar(12),
  mailClient              varchar(60),
  constraint pk_client primary key (numClient)
);



/*====================*/
/*  Table : CONTACT   */
/*====================*/

create table CONTACT (
  numContact          serial          not null,
  nomContact          varchar(50)     not null,
  telephoneContact    varchar(12)     not null,
  fonctionContact     varchar(20)     not null,
  constraint pk_contact primary key (numClient)
);


/*====================*/
/*  Table : COMMANDE  */
/*====================*/

create table COMMANDE(
  numCommande         serial          not null,
  numClient           integer         not null,
  dateCommande        date            not null,
  montantFrais        numeric(10,2)   not null,
  montantHT           numeric(10,2)   not null,
  montantTTC          numeric(10,2)   not null,
  constraint pk_commande primary key (numCommande)
);


/*==========================*/
/*  Table : DETAILCOMMANDE  */
/*==========================*/

create table DETAILCOMMANDE(
  
)


/*========================*/
/*  Table : TYPEARTICLE   */
/*========================*/

create table TYPEARTICLE(
  codeType          char(1)     not null,
  libelleType       varchar(40) null ?,
  constraint pk_typeArticle primary key (codeType)
);

insert into matski_update._TYPEARTICLE(codeType, libelleType)
  select codeType, libelleType


/*============================*/
/*  Table : DETAILSCOMMANDE   */
/*============================*/

create table DETAILSCOMMANDE (
  numCommande           integer,
  numArticle            integer,
  quantiteCommandee     integer,
  quantiteLivree        integer,
  constraint pk_detailCommande primary key (numCommandee, quantiteLivree)
);


/*======================*/
/*  Table : ETIQUETTE   */
/*======================*/

create table ETIQUETTE(
  codeEtiquette         char(3)       not null,
  libelleEtiquette      varchar(70)   not null,
  codeTypeTVA           integer       not null,
  constraint pk_etiquette primary key (codeEtiquette)
);


/*======================*/
/*  Table : LISTEPRIX   */
/*======================*/

create table LISTEPRIX(
  codeListe           char(1),
  libelleliste        varchar(20),
  constraint pk_listePrix primary key (codeListe)
);


/*======================*/
/*  Table : TARIFVENTE  */
/*======================*/

create table TARIFVENTE (

);
----------- WbStatement -----------
##sqlwb.pos=3759
##sqlwb.selStart=3759
##sqlwb.selEnd=3759
drop schema if exists matski_uptdate cascade;
create schema matski_update;
set schema 'matski_update';

/*
insert into
  matski_update.typeArticle
  (codeType, libelleType)
  
select codeType, libelleType
from matski.typeArticle
*/

/*====================*/
/*  Table : ARTICLE   */
/*====================*/
create table ARTICLE (
  numArticle        integer     not null,
  nomArticle        varchar(50) null,
  referenceInternet char(10)    null,
  codeBarre         char(13)    null,
  coutAchat         numeric     null,
  constraint pk_article primary key (numArticle)
);


/*======================*/
/*  Table : CATEGORIE   */
/*======================*/

create table CATEGORIE (
  numCategorie      int         not null,
  libelleCate       varchar(40) null,
  constraint pk_categorie primary key (numCategorie)
);


/*==================*/
/*  Table : CLIENT  */
/*==================*/

create table CLIENT (
  numClient               serial,
  nomClient               varchar(50),
  adresseRueClient        varchar(50),
  adresseCodePostalClient varchar(50),
  adresseVilleClient      varchar(40),
  adressePaysClient       varchar(30),
  telephoneClient         varchar(12),
  mailClient              varchar(60),
  constraint pk_client primary key (numClient)
);



/*====================*/
/*  Table : CONTACT   */
/*====================*/

create table CONTACT (
  numContact          serial          not null,
  nomContact          varchar(50)     not null,
  telephoneContact    varchar(12)     not null,
  fonctionContact     varchar(20)     not null,
  constraint pk_contact primary key (numClient)
);


/*====================*/
/*  Table : COMMANDE  */
/*====================*/

create table COMMANDE(
  numCommande         serial          not null,
  numClient           integer         not null,
  dateCommande        date            not null,
  montantFrais        numeric(10,2)   not null,
  montantHT           numeric(10,2)   not null,
  montantTTC          numeric(10,2)   not null,
  constraint pk_commande primary key (numCommande)
);


/*==========================*/
/*  Table : DETAILCOMMANDE  */
/*==========================*/

create table DETAILCOMMANDE(
  
)


/*========================*/
/*  Table : TYPEARTICLE   */
/*========================*/

create table TYPEARTICLE(
  codeType          char(1)     not null,
  libelleType       varchar(40) null ?,
  constraint pk_typeArticle primary key (codeType)
);

insert into matski_update._TYPEARTICLE(codeType, libelleType)
  select codeType, libelleType
  from matski.TYPEARTICLE;
 
select setval ('matski_update._article_numarticle_seq'),
  


/*============================*/
/*  Table : DETAILSCOMMANDE   */
/*============================*/

create table DETAILSCOMMANDE (
  numCommande           integer,
  numArticle            integer,
  quantiteCommandee     integer,
  quantiteLivree        integer,
  constraint pk_detailCommande primary key (numCommandee, quantiteLivree)
);


/*======================*/
/*  Table : ETIQUETTE   */
/*======================*/

create table ETIQUETTE(
  codeEtiquette         char(3)       not null,
  libelleEtiquette      varchar(70)   not null,
  codeTypeTVA           integer       not null,
  constraint pk_etiquette primary key (codeEtiquette)
);


/*======================*/
/*  Table : LISTEPRIX   */
/*======================*/

create table LISTEPRIX(
  codeListe           char(1),
  libelleliste        varchar(20),
  constraint pk_listePrix primary key (codeListe)
);


/*======================*/
/*  Table : TARIFVENTE  */
/*======================*/

create table TARIFVENTE (
  numArticle          integer,
  codeListe           char(1),
  prixVente           decimal(10,2),
  constraint pk_tarifVente primary key (numArticle, codeListe),
  constraint fk_listePrix foreign key (codeListe) 
    references LISTEPRIX (codeListe)
  constraint fk_article foreign key (numArticle)
    references ARTICLE (numArticle)
);
----------- WbStatement -----------
