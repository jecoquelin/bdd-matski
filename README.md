# bdd-matski

Ce dépôt contient une création de base de données dans un contexte spécifique

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Cette SAE nous a permis de pouvoir donner une critique sur une base de données et de pouvoir l'améliorer. Ceci nous a permis d'affiner notre connaissance en SQL.

Étapes de cette SAE 1.04 :
1. Critique de la base de données existante, proposition d'un nouveau modèle (+correction) et sa transformation en modèle relationnel
2. Répondre aux différentes requêtes
3. Extension de la base de données avec la partie "catégorie et contacts" + jeu de test pour peupler la nouvelle Base de données

## Apprentissage critique

AC 14.01 : Mettre à jour et interroger une base de données relationnelles<br>
AC 14.03 : Concevoir une base de données relationnelles à partir d'un cahier des chargesTraces : Rapport ods/pdf (avec les figures des diagrammes de classe), script 

## Langage et Framework 
  * Postgresql