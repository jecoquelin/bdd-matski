set schema
   'matski_update';



--L'ensemble des articles n'ayant jamais été commandées.
select *
from _article
except
select _article.*
from _article
natural join _detailcommande;



-- Les clients n'ayant jamais passé de commandes.
select distinct *
from _client
except
select distinct _client.* 
from _client
natural join _commande;


--L'ensemble des commandes n'ayant pas été livrées totalement

select numcommande from _detailcommande
WHERE quantitecommandee <> quantitelivree;

--Le montant total TTC commandé par type de TVA applliqué au client (France, UE sauf France, hors UE)

select codetypetva,SUM(montantttc) as "SOMME PRIX TVA" 
from _commande 
natural join _client 
natural join _etiquette 
group by codetypetva
order by codetypetva; 