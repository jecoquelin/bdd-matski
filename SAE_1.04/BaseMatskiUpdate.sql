-- Adaptation Lannion
drop schema if exists matski_update cascade;
create schema matski_update;

set schema
   'matski_update';

/*==============================================================*/
/* Table : ARTICLE                                              */
/*==============================================================*/
create table _ARTICLE (
   NUMARTICLE SERIAL not null,
   NUMCATEGORIE INTEGER not null,
   CODETYPE CHAR(1) not null,
   NOMARTICLE VARCHAR(50) null,
   REFERENCEINTERNE CHAR(10) null,
   CODEBARRE CHAR(13) null,
   COUTACHAT NUMERIC(10, 2) null,
   constraint PK_ARTICLE primary key (NUMARTICLE)
);

Insert into
   matski_update._ARTICLE (
      NUMARTICLE,
      NUMCATEGORIE,
      CODETYPE,
      NOMARTICLE,
      REFERENCEINTERNE,
      CODEBARRE,
      COUTACHAT
   )
select NUMARTICLE,
      NUMCATEGORIE,
      CODETYPE,
      NOMARTICLE,
      REFERENCEINTERNE,
      CODEBARRE,
      COUTACHAT
from matski.ARTICLE ; 

/*==============================================================*/
/* Table : CATEGORIE                                            */
/*==============================================================*/
create table _CATEGORIE (
   NUMCATEGORIE SERIAL not null,
   LIBELLECATEGORIE VARCHAR(40) null,
   constraint PK_CATEGORIE primary key (NUMCATEGORIE)
);

Insert into
   matski_update._CATEGORIE (
      NUMCATEGORIE,
      LIBELLECATEGORIE
   )
select  NUMCATEGORIE,
        LIBELLECATEGORIE
from matski.CATEGORIE ; 

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table _CLIENT (
   numClient serial,
   nomClient varchar(50),
   adresseRueClient varchar(50),
   adresseCodePostalClient varchar(50),
   adresseVilleClient varchar(40),
   adressePaysClient varchar(30),
   telephoneClient varchar(12),
   mailClient varchar(60),
   constraint pk_client primary key (numClient)
);

Insert into
matski_update._CLIENT(
      numClient,
      NOMCLIENT,
      ADRESSERUECLIENT,
      ADRESSECODEPOSTALCLIENT,
      ADRESSEVILLECLIENT,
      ADRESSEPAYSCLIENT,
      TELEPHONECLIENT,
      MAILCLIENT
   )

select numClient, 
      NOMCLIENT,
      ADRESSERUECLIENT,
      ADRESSECODEPOSTALCLIENT,
      ADRESSEVILLECLIENT,
      ADRESSEPAYSCLIENT,
      TELEPHONECLIENT,
      MAILCLIENT
from matski.CLIENT ; 

/*==============================================================*/
/* Table : COMMANDE                                             */
/*==============================================================*/
create table _COMMANDE (
   NUMCOMMANDE SERIAL not null,
   NUMCLIENT INTEGER not null,
   DATECOMMANDE DATE null,
   MONTANTFRAIS NUMERIC(10, 2) null,
   MONTANTHT NUMERIC(10, 2) null,
   MONTANTTTC NUMERIC(10, 2) null,
   constraint PK_COMMANDE primary key (NUMCOMMANDE)
);

Insert into
   _COMMANDE(
      NUMCOMMANDE,
      NUMCLIENT,
      DATECOMMANDE,
      MONTANTFRAIS,
      MONTANTHT,
      MONTANTTTC
   )

select 
   NUMCOMMANDE,
   NUMCLIENT,
   DATECOMMANDE,
   MONTANTFRAIS,
   MONTANTHT,
   MONTANTTTC
from matski.COMMANDE ; 

/*==============================================================*/
/* Table : DETAILCOMMANDE                                       */
/*==============================================================*/
create table _DETAILCOMMANDE (
   NUMCOMMANDE INTEGER not null,
   NUMARTICLE INTEGER not null,
   QUANTITECOMMANDEE INTEGER null,
   QUANTITELIVREE INTEGER null,
   constraint PK_DETAILCOMMANDE primary key (NUMCOMMANDE, NUMARTICLE)
);

INSERT INTO
   _DETAILCOMMANDE (
      NUMCOMMANDE,
      NUMARTICLE,
      QUANTITECOMMANDEE,
      QUANTITELIVREE
   )


select 
      NUMCOMMANDE,
      NUMARTICLE,
      QUANTITECOMMANDEE,
      QUANTITELIVREE
from matski.DETAILCOMMANDE ; 

/*==============================================================*/
/* Table : ETIQUETTE                                            */
/*==============================================================*/
create table _ETIQUETTE (
   CODEETIQUETTE CHAR(3) not null,
   LIBELLEETIQUETTE VARCHAR(70) null,
   CODETYPETVA INTEGER null,
   constraint PK_ETIQUETTE primary key (CODEETIQUETTE)
);

Insert into
   _ETIQUETTE (
      CODEETIQUETTE, 
      LIBELLEETIQUETTE, 
      CODETYPETVA
)

select
      CODEETIQUETTE, 
      LIBELLEETIQUETTE, 
      CODETYPETVA
from matski.ETIQUETTE ; 

/*==============================================================*/
/* Table : LISTEPRIX                                            */
/*==============================================================*/
create table _LISTEPRIX (
   CODELISTE CHAR(1) not null,
   LIBELLELISTE VARCHAR(20) null,
   constraint PK_LISTEPRIX primary key (CODELISTE)
);

insert into
   _LISTEPRIX (
      CODELISTE, 
      LIBELLELISTE
)

select
      CODELISTE, 
      LIBELLELISTE
from matski.LISTEPRIX ; 

/*==============================================================*/
/* Table : TARIFVENTE                                           */
/*==============================================================*/
create table _TARIFVENTE (
   NUMARTICLE INTEGER not null,
   CODELISTE CHAR(1) not null,
   PRIXVENTE DECIMAL(10, 2) null,
   constraint PK_TARIFVENTE primary key (NUMARTICLE, CODELISTE)
);

insert into
   _TARIFVENTE (
      NUMARTICLE, 
      CODELISTE, 
      PRIXVENTE
)

select
      NUMARTICLE, 
      CODELISTE, 
      PRIXVENTE
from matski.TARIFVENTE ; 

/*==============================================================*/
/* Table : TYPEARTICLE                                          */
/*==============================================================*/
create table _TYPEARTICLE (
   CODETYPE CHAR(1) not null,
   LIBELLETYPE VARCHAR(40) null,
   constraint PK_TYPEARTICLE primary key (CODETYPE)
);

Insert into
   _TYPEARTICLE (
      CODETYPE, 
      LIBELLETYPE
)


select
      CODETYPE, 
      LIBELLETYPE
from matski.TYPEARTICLE ; 

/*==============================================================*/
/* Table : CONTACT                                              */
/*==============================================================*/
create table _CONTACT (
   numContact serial not null,
   nomContact varchar(50) not null,
   telephoneContact varchar(12) not null,
   fonctionContact varchar(20) not null,
   numClient serial not null,
   constraint pk_contact primary key (numContact)
);

Insert into _CONTACT (
   numClient,
   nomContact ,
   telephoneContact ,
   fonctionContact
)

select
   numClient,
   NOMCONTACT1,
   TELEPHONECONTACT1,
   FONCTIONCONTACT1
from matski.CLIENT ;

Insert into _CONTACT (
   numClient,
   nomContact,
   telephoneContact ,
   fonctionContact
)

select
   numClient, 
   NOMCONTACT2,
   TELEPHONECONTACT2,
   FONCTIONCONTACT2     
from matski.CLIENT 
where nomContact2 is not null ; 

/*==============================================================*/
/* Table : seDecompose                                          */
/*==============================================================*/
create table seDecompose (
   NUMCATEGORIE SERIAL not null,
   CAT_NUMCATEGORIE2 INTEGER null,
   constraint PK_seDecompose primary key (NUMCATEGORIE)
);

/*==============================================================*/
/* Table : regroupe                                             */
/*==============================================================*/

create table regroupe (
   CODEETIQUETTE CHAR(3) not null,
   numClient serial,
   constraint PK_regroupe primary key (numClient)
);

/*==============================================================*/
/* Table : appartient                                           */
/*==============================================================*/

create table appartient (
   NUMCOMMANDE SERIAL not null,
   numClient serial,
   constraint PK_appartient primary key (NUMCOMMANDE)
);

/*==============================================================*/
/* Table : represente                                           */
/*==============================================================*/

create table represente (
   numContact serial not null,
   numClient serial,
   constraint PK_represente primary key (numContact)
);

/*==============================================================*/
/* Table : dispose                                              */
/*==============================================================*/

create table dispose (
   CODELISTE CHAR(1) not null,
   numClient serial,
   constraint PK_dispose primary key (NUMCLIENT)
);

/*==============================================================*/
/* Table : estLié                                               */
/*==============================================================*/

create table estLié (
   numArticle SERIAL not null, 
   codeType CHAR(1) not null,
   constraint pk_estLié primary key (numArticle)
);

/*==============================================================*/
/* Table : faitPartie                                           */
/*==============================================================*/

create table faitPartie (
   numArticle SERIAL not null,
   numCategorie SERIAL not null,
   constraint pk_faitPartie primary key (numArticle)
);





alter table
   _ARTICLE
add
   constraint FK_ARTICLE_ESTLIE_TYPEARTI foreign key (CODETYPE) references _TYPEARTICLE (CODETYPE);

alter table
   _ARTICLE
add
   constraint FK_ARTICLE_FAITPARTI_CATEGORI foreign key (NUMCATEGORIE) references _CATEGORIE (NUMCATEGORIE);

alter table
   seDecompose
add
   constraint FK_CATEGORI_SEDECOMPO_CATEGORI foreign key (CAT_NUMCATEGORIE2) references _CATEGORIE (NUMCATEGORIE);

alter table
   dispose
add
   constraint FK_CLIENT_DISPOSE_LISTEPRI foreign key (CODELISTE) references _LISTEPRIX (CODELISTE);

alter table
   regroupe
add
   constraint FK_CLIENT_REGROUPE_ETIQUETT foreign key (CODEETIQUETTE) references _ETIQUETTE (CODEETIQUETTE);

alter table
   appartient
add
   constraint FK_COMMANDE_APPARTIEN_CLIENT foreign key (NUMCLIENT) references _CLIENT (NUMCLIENT);

alter table
   _DETAILCOMMANDE
add
   constraint FK_DETAILCO_DETAILCOM_COMMANDE foreign key (NUMCOMMANDE) references _COMMANDE (NUMCOMMANDE);

alter table
   _DETAILCOMMANDE
add
   constraint FK_DETAILCO_DETAILCOM_ARTICLE foreign key (NUMARTICLE) references _ARTICLE (NUMARTICLE);

alter table
   _TARIFVENTE
add
   constraint FK_TARIFVEN_TARIFVENT_ARTICLE foreign key (NUMARTICLE) references _ARTICLE (NUMARTICLE);

alter table
   _TARIFVENTE
add
   constraint FK_TARIFVEN_TARIFVENT_LISTEPRI foreign key (CODELISTE) references _LISTEPRIX (CODELISTE);

alter table
   seDecompose
add
   constraint FK_NUMCATEGORIE foreign key (NUMCATEGORIE) references _CATEGORIE (NUMCATEGORIE);

alter table
   seDecompose
add
   constraint FK_PARENT foreign key (CAT_NUMCATEGORIE2) references _CATEGORIE (NUMCATEGORIE);



ALTER TABLE 
   regroupe
add 
   constraint FK_regroupe foreign key (numClient) references _CLIENT (numClient);

ALTER TABLE 
   regroupe
add 
   constraint FK_regroupe_etiquette foreign key (CODEETIQUETTE) references _ETIQUETTE(CODEETIQUETTE);

ALTER TABLE
   appartient
add 
   constraint fk_appartient_numcommande foreign key (NUMCOMMANDE) references _COMMANDE(NUMCOMMANDE);

ALTER TABLE
   appartient
add 
   constraint fk_appartient_numClient foreign key (NUMCLIENT) references _Client(numClient);

ALTER TABLE
   represente
add 
   constraint fk_represente_numClient foreign key (NUMCLIENT) references _Client(numClient);

ALTER TABLE
   represente
add 
   constraint fk_represente_numcontact foreign key (numContact) references _CONTACT(numContact);

ALTER TABLE
   dispose
add 
   constraint fk_dispose_numclient foreign key (numClient) references _CLIENT(numClient);

ALTER TABLE
   dispose
add 
   constraint fk_dispose_codeListe foreign key (codeListe) references _LISTEPRIX(codeListe);

ALTER TABLE
   estLié
add
   constraint fk_estLié_numArticle foreign key (numArticle) references _ARTICLE(numArticle);

ALTER TABLE
   estLié
add
   constraint fk_estLié_codeType foreign key (codeType) references _TYPEARTICLE(codeType);

ALTER TABLE 
   faitPartie
add
   constraint fk_faitPartie_numArticle foreign key (numArticle) references _ARTICLE(numArticle);

ALTER TABLE 
   faitPartie
add
   constraint fk_faitPartie_numCategorie foreign key (numCategorie) references _CATEGORIE(numCategorie);